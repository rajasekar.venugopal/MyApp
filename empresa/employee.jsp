<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test ="${sessionScope['session-id'] == null}">
<c:redirect url = "login.jsp"/>
</c:if>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
  <style>
    div.table {
      width : 50%
      border : 10px solid #99cccc; 
    }
  </style>
</head>
<body>
  <form method="get" action="EmployeeController">
    <button type="submit">Search</button>  
  </form>
  <div class="table">
    <table id="tableId" data-role="table" class="ui-responsive">
      <thead>
        <th> Id </th>
        <th> Name </th>
        <th> Age </th>
        <th> Mobile Number </th>
        <th> Email </th>
        <th> Designation </th>
      </thead>
      <tbody>
        <c:forEach items="${employees}" var="employee" varStatus="count">
          <tr>
          <td>${employee.getId()}</td>
          <td>${employee.getName()}</td>
          <td>${employee.getPhone()}</td>
          <td>${employee.getEmail()}</td>
          <td>${employee.getDesignation().getDesignation()}</td>
          </tr>
        </c:forEach>
      </tbody>
    </div>
  </table>
  <script>    
    $("#tableId tr").click(function() {
      alert($(this).children("td").html());
    });
  </script>
</body>
</html>
