<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test ="${sessionScope['session-id'] == null}">
<c:redirect url = "login.jsp"/>
</c:if>
<html>
  <head>
  <style>
      form {
        border: 3px solid #f1f1f1;
        width: 50%;
        padding: 12px 20px;
      }

      input[type=text], input[type=password], input[type=integer] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        border: 1px solid #ccc;
      }

      button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        font-size: 16px;
        width: 20%;
      } 

      button:hover {
        opacity: 0.8;
      }
    </style>
  </head>
  <body>
    <% Integer sessionId = (Integer) session.getAttribute("session-id"); %>
    
    <%= sessionId %>
    <center><h1>Welcome !</h1></center>
    <form action = "AuthenticationController" method = "get">
    <button type="submit" > Logout</button>
    </form>
  </body>
</html>
