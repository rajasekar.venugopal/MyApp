<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test ="${sessionScope['session-id'] != null}">
<c:redirect url = "home.jsp"/>
</c:if>
<html>
  <style>
    form {
      border: 3px solid #f1f1f1;
      width: 50%;
      padding: 12px 20px;
    }

    input[type=text], input[type=password], input[type=integer] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      border: 1px solid #ccc;
    }

    button {
      background-color: #4CAF50;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      width: 100%;
    }

    button:hover {
      opacity: 0.8;
    }
  </style>
      <% session.setAttribute("type", "login"); %>
<body>
  <center>
    <h1>Empresa</h1><br><br>
    <% String message = (String) request.getAttribute("StatusMessage"); %> 
        <% if(null != message) { %>
        <%= message %>
        <script>
            window.location("login.jsp");       
            alert(message); 
        </script>
       <% }    
    %>
    <form name = "login" method="post" action="AuthenticationController">
      <label><b>Employee id</b></label>
      <input type="integer" placeholder="Enter employee id" name="user-id" required>
      <label><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
      <button type="submit">Login</button>
    </form>
  </center>
</body>
</html>

