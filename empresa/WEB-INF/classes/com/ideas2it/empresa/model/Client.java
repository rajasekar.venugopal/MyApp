package com.ideas2it.empresa.model;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.empresa.model.Project;

/*
 * The Client is model for client entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class Client { 
    private int id;
    private String name;
    private String email;
    private int isDeleted;
    private List<Address> addresses; 
    private List<Project> projects;    

    public Client() { }

    /**
     * @param name - name of the client.
     * @param email - email address of the client is used to communicate 
     *                    the client
     */
    public Client(String name, String email) {
        this.name = name;
        this.email = email;
    }

    /**
     * @param name - name of the client.
     * @param email - email address of the client is used to communicate 
     *                    the client
     */
    public Client(String name, String email, Address address) {
        this.name = name;
        this.email = email;
        addresses = new ArrayList<>();
        addresses.add(address);
    }

    public Client(int id) {
        this.id = id;
    }

    /**
     * @param id - id of the client which is unique to every client.
     * @param name - name of the client.
     * @param email - email address of the client is used to communicate 
     *                    the client
     */
    public Client(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List getProjects() {
        return this.projects;
    }
    public void setAddress(Address address) {
        this.addresses.add(address);
    }
    
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getIsDeleted() {
        return this.isDeleted;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String  email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public String toString() {
        return "id : " + getId() + "\nName : " + getName() + "\nEmail : " 
                    + getEmail(); 
    }
}   
