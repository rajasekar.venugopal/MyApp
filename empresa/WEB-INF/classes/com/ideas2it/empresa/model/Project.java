package com.ideas2it.empresa.model;

import java.util.List;

import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.model.Employee;

/*
 * The Project is the model of project entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class Project {
    private String name;
    private String description;
    private int id;
    private int clientId;
    private int isDeleted;
    private List<Employee> employees;
    private Client client;    

    public Project() {}
    
    public Project(int id) {
        this.id = id;
    }

    public Project(int id, String name, String description, int clientId, 
        int isDeleted) 
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.clientId = clientId;
        this.isDeleted = isDeleted;
    }

    public Project(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Project(String name, String description, Client client) {
        this.client = client;
        this.name = name;
        this.description = description;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List getEmployees() {
        return this.employees;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getClientId() {
        return this.clientId;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getIsDeleted() {
        return this.isDeleted;
    }

    public int getId() {
        return this.id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    } 

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString() {
        return "\tNo : " + getId() + "\n\tName : " + getName() 
                   + "\n\tDescription : " + getDescription();
    }
}
