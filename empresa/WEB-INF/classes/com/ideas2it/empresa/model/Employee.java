package com.ideas2it.empresa.model;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.model.Address;

/*
 * The Employee is the model of employee entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class Employee {
    private int id;
    private String name; 
    private String email;
    private String phone;
    private String DOB;
    private String password;
    private Designation designation;
    private List<Project> projects; 
    private List<Address> addresses;  
    private int isDeleted;  

    /** 
     * @param name  name to be added. 
     * @param email  email address of the employee which is unique and can be 
     *                    used to communicate the employee. 
     * @param phone phone number of the employee which is unique and can be 
     *                    used to communicate the employee. 
     * @param DOB DOB to be added. 
     *                           designation. 
     */

    public Employee(String name, String email, String phone, String DOB) 
    {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.DOB = DOB;
    }

    public Employee(int id) {
        this.id = id;
    }

    public Employee(String name, String email, String phone, String DOB, 
        Designation designation) 
    {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.DOB = DOB;
        this.designation = designation;
    }

    public Employee(String name, String email, String phone, String DOB, 
        Designation designation, Address address) 
    {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.DOB = DOB;
        this.designation = designation;
        addresses = Arrays.asList(address);
        address.setEmployee(this);
    }
   
    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getIsDeleted() {
        return this.isDeleted;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee() {
    }

    public void setAddress(Address address) {
        this.addresses.add(address);
    }
    
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public List<Project> getProjects() {
        return this.projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
    
    public void setProject(Project project) {
        this.projects.add(project);
    }    

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
  
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }
        
    public String toString() {
        return "\tNo : " + getId()
                                  + "\n\tName : " + getName() 
                                  + "\n\tDOB : " + getDOB() 
                                  + "\n\tEmail : " + getEmail()
                                  + "\n\tPhone : " + getPhone();
    } 
}
