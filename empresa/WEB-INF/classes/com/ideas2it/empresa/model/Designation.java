package com.ideas2it.empresa.model;

/*
 * The Designation is a model designation entity.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class Designation{
    private int id;
    private String designation;

    public Designation() {}

    /**
     * @param id id of the designation which is unique to every designation.
     * @param designation designation of an employee
     */
    public Designation(int id) {
        this.id = id;
    }    

    /**
     * @param id id of the designation which is unique to every designation.
     * @param designation designation of an employee
     */
    public Designation(int id, String designation) {
        this.id = id;
        this.designation = designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return this.designation;
    } 

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    } 
}
