package com.ideas2it.empresa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;

import com.ideas2it.empresa.common.EmpresaLogger;    
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Employee; 
import com.ideas2it.empresa.service.EmployeeService;
import com.ideas2it.empresa.service.impl.EmployeeServiceImpl;

public class EmployeeControllerServlet extends HttpServlet { 
    private static EmployeeService employeeServiceImpl = new 
        EmployeeServiceImpl();    
    
    public void init() throws ServletException {
        System.out.println("initiated");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException 
    {
        getAllEmployees(request, response);
    }  

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException 
    {   

    }  

    public void destroy() {
        
    }

    /**
     * <p>Gets all employees from record and returns the list of all employees.
     * @throws AppException if employees cannot be retrieved.
     * @return List of all employee objects 
     */
    public void getAllEmployees(HttpServletRequest request, 
        HttpServletResponse response)
    {
        try {
            request.setAttribute("employees", 
                    employeeServiceImpl.getAllEmployees());
            RequestDispatcher requestDispatcher =
                            request.getRequestDispatcher("employee.jsp");
            requestDispatcher.forward(request, response);
        } catch (AppException appException) {
            EmpresaLogger.log(appException);
        } catch (ServletException e) {
            EmpresaLogger.log(e);            
        } catch (IOException e) {
            EmpresaLogger.log(e);            
        }
    }
}
