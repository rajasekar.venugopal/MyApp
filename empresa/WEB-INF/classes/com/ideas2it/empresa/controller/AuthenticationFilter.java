package com.ideas2it.empresa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;

public class AuthenticationFilter implements Filter {
    
    public void init(FilterConfig config) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response,
                     FilterChain chain) throws IOException, ServletException 
    {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        if (null != session) {
            String type = (String) session.getAttribute("type");
            System.out.println("\n\n\n\n " + type);
            if(type.equalsIgnoreCase("login") || type.equalsIgnoreCase("user")) 
            {
                chain.doFilter(httpRequest, httpResponse);                
            } 
        } else {
            httpResponse.sendRedirect("index.html");
            System.out.println("\n\n\n\n\n\nredirecting"); 
        } 
    }

    public void destroy() {

    }
}
