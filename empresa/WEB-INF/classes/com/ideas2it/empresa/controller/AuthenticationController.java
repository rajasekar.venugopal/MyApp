package com.ideas2it.empresa.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;

import com.ideas2it.empresa.common.EmpresaLogger;    
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Employee; 
import com.ideas2it.empresa.service.EmployeeService;
import com.ideas2it.empresa.service.impl.EmployeeServiceImpl;

public class AuthenticationController extends HttpServlet { 
    private static EmployeeService employeeServiceImpl = new 
        EmployeeServiceImpl();    
    public void init() throws ServletException {
        System.out.println("initiated");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException 
    {
        logout(request, response); 
    }  

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException 
    {   
        login(request, response); 
    }  

    /**
     * Takes employee credentials and logs employee in if credentials are valid.
     */
    public void login(HttpServletRequest request, HttpServletResponse response) 
    {
        int id = Integer.parseInt(request.getParameter("user-id"));
        String password = (String) request.getParameter("password");
        Employee employee = new Employee(id); 
        employee.setPassword(password);
        System.out.println(id + "********");
        System.out.println(password + "******");
        try { 
            if (employeeServiceImpl.validateEmployeeCredentials(employee)) {
                response.getWriter().write("<h1>success</h1>");
                request.getSession().setAttribute("session-id", id);    
                request.getSession().setAttribute("type", "user");
                System.out.println("success");
                response.sendRedirect("home.jsp");
            } else {    
                System.out.println("failure");
                request.setAttribute("StatusMessage", "Username or password is" 
                                        + " wrong.");
                RequestDispatcher requestDispatcher =
                                request.getRequestDispatcher("login.jsp");
                requestDispatcher.include(request, response);
            }        
        } catch (AppException e) {
            EmpresaLogger.log(e);            
        } catch (DBException e) {
            EmpresaLogger.log(e);            
        } catch (IOException e) {
            EmpresaLogger.log(e);
        } catch (ServletException e) {
            EmpresaLogger.log(e);
        }
    }    

    /**
     * Logouts employee from the application
     */
    public void logout(HttpServletRequest request, HttpServletResponse response) 
    {
        try { 
            request.getSession().invalidate();
            response.sendRedirect("login.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
