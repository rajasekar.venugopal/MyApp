package com.ideas2it.empresa.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.common.EmpresaLogger;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.service.ClientService;
import com.ideas2it.empresa.service.impl.ClientServiceImpl;
import com.ideas2it.empresa.service.impl.ProjectServiceImpl;
import com.ideas2it.empresa.service.ProjectService;

/**                                                                                
 * The ProjectController gets users inputs controls the logic and handles the 
 * user request then responds the user with the result for the operation for 
 * the requested operation. 
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class ProjectController {
    private ProjectService projectServiceImpl = new ProjectServiceImpl();

    /**
     * <p>Adds project to the projects record and returns the result. Returns 
     * success message if added successfully otherwise error message.    
     * @param name - name of the project. 
     * @param description - description about the project to be added.
     * @param clientId id of the client which is unique for every client.
     * @return status - Success message if added successfully, failure                                              
     *                          message other wise.                  
     */
    public Map<String, Object> addProject(String name, String description, 
        int clientId) 
    {
        Map<String, Object> status = new HashMap<>();
        try {
            status = projectServiceImpl.addProject(name, description, clientId);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            status.put(Constant.MESSAGE, "Something went wrong insertion " 
                          + "failed.");
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
        }
        return status;
    }

    /**
     * <p>Gets all projects from record and returns the list of all projects.
     * @return List of all project objects 
     * @throws AppException if pojects cannot be retrieved
     */
    public List<Project> getAllProjects() throws AppException {
        List<Project> projects = new ArrayList<>();
        try {
            projects = projectServiceImpl.getAllProjects();
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't fetch "
                                           + "project records");
        }  
        return projects;
    } 

    /**
     * Gets Employee for the given employee id and returns it.
     * @param id id of the employee which is unique for every employee
     * @return employee employee for the given id 
     * @throws AppException if employee cannot be retrieved or not found for the
     *         given id.
     */
    public Project getProjectById(int id) throws AppException {
        Project project = null;
        try {
            project = projectServiceImpl.getProjectById(id);
        } catch (DBException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't get employee."
                                    );              
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("No project found for the given id.");        
        }
        return project;
    } 

    /**
     * <p>Gets a particular project from the projects record identified by the 
     * project's name and returns the project.
     * @param name name of the project to be searched.
     * @param clientId id of the client which is unique for every client.
     * @return success or failure with project object if project is found with 
                    the project and client id.
     * @throws AppException if cannot retrive all projects.                 
     */
    public Map<String, Object> getProjectByName(String name, int clientId) 
        throws AppException 
    {
        Map<String, Object> status = new HashMap<>(); 
        try {
            status = projectServiceImpl.getProjectByName(name, clientId);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't fetch "
                                           + "project records");
        }  
        return status; 
    }

    /**
     * <p>Gets a particular project from the projects record identified by the 
     * project's id and returns the project.
     * @param projectId id of the project used to identify the project.
     * @param clientId id of the client which is unique for every client.
     * @return success or failure with projetc object project is found with the project and client id.
     * @throws AppException if cannot retrive all projects.                 
     */
    public Map<String, Object> getProjectById(int projectId, int clientId) 
        throws AppException 
    {
        Map<String, Object> status = new HashMap<>(); 
        try {
            status = projectServiceImpl.getProjectById(projectId, clientId);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't fetch "
                                           + "project records");
        }  
        return status; 
    }

    /**
     * <p>Gets a particular project from the projects from a particular client 
     * identified by the client's id table and returns it as a list.
     * @param clientId id of the client which is unique for every client.
     * @return list of projects client owns.
     * @throws AppException if cannot retrive all projects.                 
     */
    public List<Project> getAllProjectsByClientId(int clientId) throws 
        AppException 
    {
        List<Project> projects = new ArrayList<>();
        try {
            projects = projectServiceImpl.getAllProjectsByClientId(clientId);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't fetch "
                                           + "project records");
        }  
        return projects; 
    }

    /**
     * <p>Deletes project from record identified by the project id.
     * @param id - id of the project to be deleted.
     * @return status - status message whether deleted or not.  
     */
    public String deleteProject(int id) {
        String status = new String();
        try {
            status = projectServiceImpl.deleteProject(id);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            status = "Something went wrong project with the id = " + id 
                           + "not deleted";
        }
        return status;
    }
    
    /**
     * <p>Checks if project is existing with the id and returns a map with error 
     * state either success or failure and corresponding message.  
     * @param id - id of the project being searched 
     * @return status - map with success or failure state and coressponding 
     *                      success or failure message.
     */
    public Map<String, String> hasProjectForId(String id) {
        Map<String, String> status = new HashMap<>();
        try {
            status = projectServiceImpl.hasProjectForId(id);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            status.put(Constant.ERROR_STATE, "failure");
            status.put(Constant.MESSAGE, "Somthing went wrong couldn't get " 
                          + "project for the id = " + id);
        }
        return status;
    }

    /**
     * <p>Assigns employee to the project.
     * @param project prooject in which employee ro be assigned
     * @param employee employee to be assigned in the project.
     * @return message when employee is assigned to project successfully.
     * @throws AppException if employee cannot be assigned to project.
     */
    public String assignProjectToEmployee(Project project, Employee employee) 
        throws AppException 
    {
        String message = new String();
        try {
            message = projectServiceImpl.assignProjectToEmployee(project, 
                                             employee);
        } catch(DBException e) {
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong couldn't assign " 
                                      + "employee to project");
        }
        return message;
    }

    /**
     * <p>Checks no of projects a client is owning and returns the no of  
     * projects of a client.
     * @param clientId id of the client which is unique to every client 
     * @return no of projects the client is owning.
     * @throws AppException if the no of projects for a client cannot be checked 
     */
    public int noOfProjectByClientId(int clientId) throws AppException {
        int noOfProjects = 0;         
        try {
            noOfProjects = projectServiceImpl.getNoOfProjects(clientId);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't get no of " 
                                       + "projects ");            
        }
        return noOfProjects;
    }
}
