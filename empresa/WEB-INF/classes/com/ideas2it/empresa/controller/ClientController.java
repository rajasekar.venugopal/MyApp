package com.ideas2it.empresa.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.common.EmpresaLogger;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.service.ClientService;
import com.ideas2it.empresa.service.impl.ClientServiceImpl;

/**                                                                                
 * The ClientController gets users inputs controls the logic and handles the 
 * user request then responds the user with the result for the operation for 
 * the requested operation. 
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public class ClientController {
    private static ClientService clientServiceImpl = new ClientServiceImpl();

    /**
     * Adds client to the clients record and returns the result. Returns 
     * success message if added successfully otherwise error message.    
     * @param client client to be added to the client record
     * @return status Success message if added successfully, failure                                              
     *     message other wise.                  
     */
    public Map<String, Object> addClient(Client client) throws 
        AppException
    {
        Map<String, Object> status = new HashMap<>();
        try {
            status = clientServiceImpl.addClient(client);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            status.put(Constant.MESSAGE, "Something went wrong insertion " 
                          + "failed.");
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
        } catch (DBException e) {
            throw new AppException("Something went wrong insertion failed.");
        }
        return status;
    }

    /**
     * Gets all clients from record and returns the list of all clients.
     * @return List of all client objects 
     * @throws AppException if projects cannot be retrieved
     */
    public List<Client> getAllClients() throws AppException {
        List<Client> clients = new ArrayList<>();
        try {
            clients = clientServiceImpl.getAllClients();
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException(e.getMessage());
        }  
        return clients;
    } 

    /**
     * Deletes the given client from the client record. 
     * @return message Success message when deleted successfully.  
     */
    public String deleteClientById(Client client) throws AppException {
        String message = new String();
        try {
            message = clientServiceImpl.deleteClient(client);
        } catch (DBException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong client with the id = " 
                        + client.getId() + "not deleted");              
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Client not deleted.");        
        }
        return message;
    }

    /**
     * Updates client email to the client record and returns success message
     * if updated successfully otherwise error message.   
     * @param id - id of the client which is unique to every client. 			
     * @param email - new name to be updated.
     * @return status - Success message if updated successfully, failure 
     *                      message other wise.
     */
    public String updateEmail(Client client) throws AppException {
        String message = new String();
        try {
            message = clientServiceImpl.updateEmail(client);
        } catch (DBException e) {            
            EmpresaLogger.log(e);
            throw new AppException("Something went wrong " + client.getEmail() 
                                       + " was not updated.");
        } catch(AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Not Updated.");             
        }
        return message;
    }

    /**
     * Checks if the client is existing with the id and returns a map with error 
     * state either success or failure and corresponding message.  
     * @param id - id of the client being searched 
     * @return status - map with success or failure state and corresponding 
     *                      success or failure message.
     */
    public Map<String, String> hasClientForId(String id) {
        Map<String, String> status = new HashMap<>();
        try {
            status = clientServiceImpl.hasClientForId(id);
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            status.put(Constant.ERROR_STATE, "failure");
            status.put(Constant.MESSAGE, "Something went wrong couldn't get " 
                          + "client for the id = " + id);
        } catch (DBException e) {
            e.printStackTrace();        //TODO to be removed
        }
        return status;
    }


    /**
     * Gets client for the given client id and returns it.
     */
    public Client getClientById(int id) throws AppException {
        Client client = null;
        try {
            client = clientServiceImpl.getClientById(id);
        } catch (DBException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("Something went wrong couldn't get client.");              
        } catch (AppException e) {
            EmpresaLogger.log(e);                          
            throw new AppException("No client found for the given id.");        
        }
        return client;
    }
}
