package com.ideas2it.empresa.view;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ideas2it.empresa.exception.AppException;             
import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.controller.ClientController;
import com.ideas2it.empresa.controller.EmployeeController;           
import com.ideas2it.empresa.controller.ProjectController;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;

/**
 * <p>The ProjectView acts as an interface to the user and provides the user 
 * with the options for the operations. It gets input from the user for the  
 * operations then responds to the user with the results.
 *  
 * @author Rajasekar
 * created on 14/07/2017
 */
public class ProjectView {
    private static Scanner scanner = new Scanner(System.in);
    private ProjectController projectController;

    /**
     * Inserts and deletes from project record. Reads all projects from the 
     * project record and displays it. Gets employees in a particular project. 
     */
    public void processProjects() {
        ProjectView projectView = new ProjectView();
        String choice ;
        System.out.println("\nEnter your choice : \n1.Add new " 
                              + "project:\n2.Get all projects\n3.Delete " 
                              + "project \n4.Assign employee to project "  
                              + "\n5.Print employee in a project" 
                              + "\n6.Exit" );
        choice = scanner.next();
        if (CommonUtil.isNumeric(choice)) {    
            switch (Integer.parseInt(choice)) {
                case 1:  
                    projectView.addProject();
                    break;
                case 2:
                    projectView.displayAllProjects();
                    break;
                case 3:
                    projectView.deleteProject();
                    break;
                case 4:
                    projectView.assignProjectToEmployee();
                    break;
                case 5:
                    projectView.getEmployeesByProject();
                    break;
                case 6:
                    System.exit(Constant.ZERO);
                default:
                    System.out.println("Wrong choice !"); 
            }               
        } else {
            System.out.println(Constant.INVALID_NO);
        }
    }

    /**
     * Takes client id and gets a particular project from the projects record 
     * identified by the project's name and display the project.
     *
     * @param clientId - id of the client which is unique and used to identify a 
     *                       particular client.
     */
    public void displayProjectByName(int clientId) {
        ProjectView projectView = new ProjectView();
        projectController = new ProjectController();

        System.out.println("Enter project name :");
        String name = scanner.next();
        if(CommonUtil.isLiteral(name)) {
            try {
                Map<String, Object> result = projectController.getProjectByName(
                        name, clientId);
                if(result.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) {
                    Project project = (Project) result.get(Constant.PROJECT);
                    System.out.println(project);
                    projectView.getEmployeesByProject(String.valueOf(
                        project.getId()));
                } else {
                    System.out.println(result.get(Constant.MESSAGE));
                }
            } catch (AppException appException) {
                System.out.println(appException.getMessage());
            }
        } else {
            System.out.println(Constant.INVALID_NO);    
        }    
    }

    /**
     * Gets all projects from a particular client and displays identified by the
     * client id. 
     * 
     * @param clientId - id of the client which is unique and used to identify a 
     *                       particular client.
     */
    public void displayAllProjectsByClient(int clientId) {
        projectController = new ProjectController();
        try {
            for (Project project : projectController.getAllProjectsByClientId(
                clientId)) 
            {
                System.out.println(project);
            }
        } catch (AppException appException) {
            System.out.println(appException.getMessage());
        } 
    }

    /**
     * Gets a particular project from the projects record identified by the 
     * project's id and displays the project.
     * 
     * @param clientId - id of the client which is unique and used to identify a 
     *                       particular client.
     */
    public void displayProjectById(int clientId) {
        projectController = new ProjectController();
        ProjectView projectView = new ProjectView();
        System.out.println("Enter project id :");
        String projectId = scanner.next();
        if(CommonUtil.isNumeric(projectId)) {
            try {
                Map<String, Object> result = projectController.getProjectById(
                        Integer.parseInt(projectId), clientId);

                if(result.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) {
                    System.out.println((Project) result.get(Constant.PROJECT));
                    projectView.getEmployeesByProject(projectId);
                } else {
                    System.out.println(result.get(Constant.MESSAGE));
                }
            } catch (AppException appException) {
                System.out.println(appException.getMessage());
            }
        } else {
            System.out.println(Constant.INVALID_NO);    
        }  
    }

    /**
     * Gets project details and adds an new project to the project record for 
     * the given client. 
     *
     * @param clientId - id of the client which is unique and used to identify a 
     *                       particular client.
     */
    public void addProject(int clientId) {
        ProjectView projectView = new ProjectView();
        projectController = new ProjectController();
        System.out.println("\nEnter project name");
        String name = scanner.next();
        System.out.println("\nEnter project description");
        String description = scanner.next();
        Map<String, Object> status = projectController.addProject(name, 
                                         description, clientId);
        System.out.println(status.get(Constant.MESSAGE));    
    }

    /**
     * Gets all project records from the employee record and displays. 
     */
    public void displayAllProjects() {
        projectController = new ProjectController();
        try {
            List<Project> projects = projectController.getAllProjects();
            for(Project project : projects) {
                System.out.println(project);
            }
        } catch (AppException appException) {
            System.out.println(appException.getMessage());
        }
    }

    /**
     * Gets project details and and client id then validates whether client is 
     * existing if existing adds project to the project record and displays the 
     * status message else displays error message. 
     */
    private void addProject() {
        ClientController clientController = new ClientController();
        ProjectView projectView = new ProjectView();
        projectController = new ProjectController();
  
        System.out.println("\nEnter project name");
        String name = scanner.next();
        System.out.println("\nEnter project description");
        String description = scanner.next();
        System.out.println("\nEnter client id");
        String clientId = scanner.next();

        if(CommonUtil.isNumeric(clientId)) {
            Map<String, String> status = clientController.hasClientForId
                                                              (clientId);
            if(Constant.SUCCESS.equals(status.get(Constant.ERROR_STATE))) {
                Map<String, Object> result = projectController.addProject(name, 
                                                 description, 
                                                 Integer.parseInt(clientId));
                System.out.println(result.get(Constant.MESSAGE));                
            } else {
                System.out.println(status.get(Constant.MESSAGE));
            }
        } else {
            System.out.println(Constant.INVALID_NO);
            projectView.addProject();
        }          
    }

     /**
      * Gets project id and deletes a project from the project record and 
      * displays the success message if deleted, else displays error message.
      */
    private void deleteProject() {
        projectController = new ProjectController();
        ClientView clientView = new ClientView();
        ClientController clientController = new ClientController(); 
        Map<String, String> clientStatus = clientView.checkClientStatus();

        if(clientStatus.get(Constant.ERROR_STATE).equals(Constant.FAILURE)) {
            System.out.println(clientStatus.get(Constant.MESSAGE));
        } else if (clientStatus.get(Constant.ERROR_STATE).equals(
            Constant.SUCCESS) && Integer.parseInt(clientStatus.get(
            Constant.NO_OF_PROJECTS)) > 1) 
        {
            System.out.println("\nEnter project id");
            String id = scanner.next();
            this.deleteProjectById(id);
        } else {
            System.out.println("Client has only one project you can only delete"    
                                   + " along with the deletion of client \n Do" 
                                   + " you want to proceed enter yes / no ");
            String choice = scanner.next();
            /*if(choice.equalsIgnoreCase(Constant.YES)) {
                System.out.println(clientController.deleteClientById(
                                       Integer.parseInt(clientStatus.get(
                                        Constant.CLIENT_ID))));
            }*/
        }
    }

    /**
     * Takes project id and deletes the project from project record and displays
     * the result.
     *
     * @param id - id of the project which is unique for every project
     */
    private void deleteProjectById(String id) {
        projectController = new ProjectController();
        Map<String, String> status = projectController.hasProjectForId(id);

        if(status.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) { 
            System.out.println(projectController.deleteProject(
                                   Integer.parseInt(id)));
        } else {
            System.out.println(status.get(Constant.MESSAGE));
        }
    }     

    /**
     * Gets project id and checks whether project is existing if not, displays 
     * error message, otherwise takes project id and adds employee to the 
     * project.
     */
    private void assignProjectToEmployee() {
        projectController = new ProjectController();
        EmployeeController employeeController = new EmployeeController();
        EmployeeView employeeView = new EmployeeView();
        System.out.println("Enter project id :");
        String projectId = scanner.next();
        System.out.println("Enter employee id :");
        String employeeId = scanner.next();
        if(CommonUtil.isNumeric(projectId) && CommonUtil.isNumeric(employeeId))
        {
            try {
                Employee employee = employeeController.getEmployeeById(
                                        Integer.parseInt(employeeId));
                Project project = projectController.getProjectById(
                                      Integer.parseInt(projectId));
                System.out.println(employee);
                System.out.println(project);
                projectController.assignProjectToEmployee(project, employee);
            } catch (AppException e) {
                System.out.println(e.getMessage());        
            }
        } else {
            System.out.println(Constant.INVALID_NO);
        }
    }

    /**
     * Takes employee id and project id then checks whether project and employee
     * is existing and assigns employee to project and displays error or success
     * message.
     *
     * @param projectId project id to which employee to be assigned.
     */
    private void assignProjectToEmployee(int projectId) {
        EmployeeController employeeController = new EmployeeController();

        System.out.println("Enter employee id :");
        String employeeId = scanner.next();
        Map<String, String> status = 
                                employeeController.getEmployeeById(employeeId);

        if(status.get(Constant.ERROR_STATE).equals(Constant.FAILURE)) {
            System.out.println(status.get(Constant.MESSAGE));
        } else {
            String statusMessage = employeeController.assignProjectToEmployee(
                                   Integer.parseInt(employeeId), projectId);    
            System.out.println(statusMessage);
        }         
    }

    /**
     * Gets project id and checks if project is existing and displays all 
     * employees else displays error message.
     */        
    private void getEmployeesByProject() {
        ProjectView projectView = new ProjectView();
        System.out.println("Enter project id :");
        String projectId = scanner.next();

        if(CommonUtil.isNumeric(projectId)) {
            projectView.getEmployeesByProject(projectId); 
        } else {
            System.out.println(Constant.INVALID_NO);
        }
    }

    /**
     * Gets all employees in a particular project identified by the given 
     * project id.  
     *
     * @param projectId - id of the project which is unique to every project.
     */        
    private void getEmployeesByProject(String projectId) {
        EmployeeController employeeController = new EmployeeController();
        projectController = new ProjectController();
        try {
            Map<String, String> status = 
                                   projectController.hasProjectForId(projectId);
            if(status.get(Constant.ERROR_STATE).equals(Constant.SUCCESS)) {
                List<Employee> employees = 
                                      employeeController.getEmployeesByProject(
                                      Integer.parseInt(projectId));
                if(!employees.isEmpty()) {
                    System.out.println("Employees in the project :");
                    for(Employee employee : employees) {
                        System.out.println("\n\t" + employee.getId() + "\n\t"
                                              + employee.getName() + "\n\t" 
                                              + employee.getEmail());
                    }
                }
            } else {
                System.out.println(status.get(Constant.MESSAGE));
            }
        } catch (AppException appException) {
            System.out.println(appException.getMessage());
        }  
    }
}
