package com.ideas2it.empresa.dao;

import java.util.List;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;

/**
 * The AddressDAO exposes the create, update operations related to the address.
 * 
 * @author Rajasekar
 * created on 19/07/2017
 */
public interface AddressDAO { 

    /**
     * Adds address to address record and returns one if added successfully 
     * else zero. 
     * @param address - address, postal address of the owner which can be used  
     *                  to communicate him / her.
     * @return result - returns id of the address if added successfully else 
                        zero. 
     * @throws AppException if the address cannot be added to the owner. 
     */
    int addAddress(Address address) throws AppException;

    /**
     * Updates address to record with the new address of the owner and returns 
     * one if updated successfully else zero.
     * @param address postal address of the owner which can be use to
     *              communicate him / her.
     * @return result - returns one if address is updated successfully else zero
     * @throws AppException if the address cannot be updated.
     */
    int updateAddress(Address address) throws AppException;

    /**
     * Checks whether the given type of the address is available for the    
     * particular owner and returns true if available else false.
     * @param address  postal address of the owner which can be use to
     *              communicate him / her.
     * @return true if record is found matching the given clause else false
     * @throws AppException if type cannot be checked.
     */
    boolean isTypeExists(Address address) throws AppException;
}

