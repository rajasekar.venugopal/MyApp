package com.ideas2it.empresa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.cfg.Configuration;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.empresa.common.ConnectionFactory;
import com.ideas2it.empresa.common.SessionGenerator;
import com.ideas2it.empresa.dao.ProjectDAO;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Project;


/**
 * The ProjectDAOImpl is the implementation of ProjectDAO interface it  
 * implements all methods to interact with the record and acts as an  
 * intermediate for retrieving and storing data into or from the record.
 *
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class ProjectDAOImpl implements ProjectDAO {
 	private SessionFactory factory = SessionGenerator.getFactory();    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#insert(Project)
     */
    @Override
    public int insert(Project project) throws AppException {
        int id = 0;
        try (Session session = factory.openSession()) {
            System.out.println(factory);
            Transaction transaction = session.beginTransaction();
            id = (Integer) session.save(project);
            transaction.commit();
            session.close();           
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to insert project"
                                       , e); 
        }
        return id;                                                   
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#retriveAllProjects()
     */
    @Override
	public List<Project> retriveAllProjects() throws AppException {
		List<Project> projects = new ArrayList<Project>();
        System.out.println(factory);
		try (Session session = factory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Project> query = builder.createQuery(Project.class);
            Root<Project> projectRoot = query.from(Project.class);
            query.select(projectRoot);
            projects = session.createQuery(query).getResultList();     
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to retrieve all " 
                                      + "projects ", e);           
        }
		return projects;
	}
    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#delete(int)
     */
    @Override
    public int delete(Project project) throws AppException {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Project deletedProject = (Project) session.merge(project);
            transaction.commit();            
        } 
        catch (HibernateException e) {
            throw new AppException("Exception while trying to delete by id = " 
                                       + project.getId(), e);
        }
        return 1;   //TODO change return
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#retrieveAllProjects(int)
     */
    @Override
    public List<Project> retrieveAllProjects(int clientId) throws AppException {
        SessionFactory factory = null; 
        List<Project> projects = null;
        Session session = null;
        int result = 0;
		try {
	        factory = SessionGenerator.getFactory();
		    session = factory.openSession();
            Criteria criteria = session.createCriteria(Project.class).add(
                                    Restrictions.eq("client.id", clientId));
            criteria.add(Restrictions.eq("isDeleted", 0));
            projects = criteria.list();
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to retrieve all " 
                                      + "projects of client = " + clientId, 
                                      e);            
        }
        finally {            
            session.close();
        } 
		return projects;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#retrieveProjectById(int, 
     *       int)
     */
    @Override
    public Project retrieveProjectById(int projectId, int clientId) 
        throws AppException 
    {
        Project project = new Project();
		try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("id", projectId));
            criteria.add(Restrictions.eq("client.id", clientId));
            project = (Project) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new AppException("Exception while trying to read with id = " 
                                       + projectId, e);
        }
        return project;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#retrieveProjectByName(
     *      String, int)
     */
    @Override
    public Project retrieveProjectByName(String name, int clientId) 
        throws AppException 
    {
        Project project = new Project();
		try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("name", name));
            List<Project> projects = criteria.list();
            System.out.println(projects);             
        } catch (HibernateException e) {
            throw new AppException("Exception while trying to read with name = " 
                                       + name, e);
        }
        return project;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#isProjectExists(int)
     */
    @Override
    public boolean isProjectExists(Project project) throws AppException {
        boolean isExists = false;	    
        SessionFactory factory = null; 
        Session session = null;
        try {
	        factory = SessionGenerator.getFactory();
		    session = factory.openSession();
            Criteria criteria = session.createCriteria(Project.class);
            List projects = criteria.list(); 
            if(!projects.isEmpty()) {
                isExists = true;
            }
        } catch (HibernateException e) {
            throw new AppException("Exception while trying to read with id = " 
                                       + project.getId(), e);
        }
        finally {
            session.close();            
        }
        return isExists;    
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#assignProjectToEmployee(Project)                                
     */
    @Override
    public void assignProjectToEmployee(Project project) throws DBException {
        Transaction transaction = null;        
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            session.update(project);
            transaction.commit();   
        } catch(HibernateException e) {
            transaction.rollback();
            throw new DBException("Exception while trying to assign employee to" 
                                      + " the project " + project.getId(), e);
        }           
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#getProjectById(int)                                
     */
    @Override
    public Project getProjectById(int id) throws DBException {
        Project project = null;
        try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("id", id));
            criteria.add(Restrictions.eq("isDeleted", 0));
            project = (Project) criteria.uniqueResult();
        } catch(HibernateException e) {
            throw new DBException("Exception while trying to retrieve Project " 
                                            + "with id " + id, e);
        }
        return project;  
    }


    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ProjectDAO#getNoOfProjects(int)
     */
    @Override
    public int getNoOfProjects(int clientId) throws AppException {
        SessionFactory factory = null; 
        Session session = null;
        int result = 0;
		try {
	        factory = SessionGenerator.getFactory();
		    session = factory.openSession();
            Criteria criteria = session.createCriteria(Project.class).add(Restrictions.eq("clientId", clientId));
            List<Project> projects = criteria.list();
            result = projects.size();
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to retrieve all " 
                                      + "projects of client = " + clientId, e);            
        }
        finally {            
            session.close();
        } 
		return result;        
    }
}
