package com.ideas2it.empresa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.common.ConnectionFactory;
import com.ideas2it.empresa.common.SessionGenerator;
import com.ideas2it.empresa.dao.EmployeeDAO;
import com.ideas2it.empresa.model.Address;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;

/**
 * The EmployeeDAOImpl is the implementation of EmployeeDAO interface it 
 * implements all methods to interact with the record and acts as an 
 * intermediate for retrieving and storing datas into or from the record.
 *
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class EmployeeDAOImpl implements EmployeeDAO {
	private Connection connection;
	private Statement statement;
    private static SessionFactory factory = SessionGenerator.getFactory(); 

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#retrieveEmployees()                
     */
    @Override
	public List<Employee> retrieveEmployees() throws AppException {
		List<Employee> employees = new ArrayList<Employee>();
		try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("isDeleted", 0));
            employees = criteria.list();
            Employee employee = employees.get(0);
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to retrieve all " 
                                      + "employees ", e); 
        }
		return employees;
	}

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#insert(Employee)                                
     */
    @Override
    public int insert(Employee employee) throws DBException {
        Transaction transaction = null;
        Integer employeeId = 0;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            Address address = new Address();
            address.setEmployee(employee);
            System.out.println("persist");
            //session.persist(employee);
            System.out.println("save");
            session.save(employee);
            session.flush();            
            transaction.commit();         
        } 
        catch (HibernateException e) {
            transaction.rollback();
            throw new DBException("Exception while inserting ", e);
        }         
        return employeeId; 
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#delete(int)                                
     */
    @Override
    public void delete(Employee employee) throws DBException {   
        Transaction transaction = null;        
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            employee.setIsDeleted(1);
            for(Project project : employee.getProjects()) {
                project.setIsDeleted(1);
            }
            session.merge(employee);
            transaction.commit();   
        } catch(HibernateException e) {
            transaction.rollback();
            throw new DBException("Exception while updating name for id =" 
                                      + employee.getId() + "with name = " 
                                      + employee.getName(), e);
        } 
    }
    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#updateNameById(int, String)                                
     */
    @Override
    public void updateNameById(Employee employee) throws DBException {
        Transaction transaction = null;        
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            session.merge(employee);
            transaction.commit();   
        } catch(HibernateException e) {
            transaction.rollback();
            throw new DBException("Exception while updating name for id =" 
                                      + employee.getId() + "with name = " 
                                      + employee.getName(), e);
        }       
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#updateEmailById(int, 
     *      String)                                 
     */
    @Override
    public void updateEmailById(Employee employee) throws DBException {
        Transaction transaction = null;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            Employee updatedEmployee = (Employee) session.merge(employee);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw new DBException("Exception while updating email for id " 
                                       + employee.getId() + "with email = " 
                                       + employee.getEmail(), e);
        }
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#updatePhoneById(int, 
     *      String)                
     */
    @Override
    public void updatePhoneById(Employee employee) throws DBException {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.merge(employee);
            transaction.commit();   
        } catch(HibernateException e) {
            throw new DBException("Exception while updating name for id =" 
                                      + employee.getId() + "with name = " 
                                      + employee.getPhone(), e);
        } 
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#readByEmail(String)                                
     */
    @Override
    public Employee getEmployeeByEmail(String email) throws DBException {
        Employee employee = null;
        try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("email", email));
            employee = (Employee) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DBException("Exception while getting employee with email" 
                                      + email, e);
        }
        return employee;                
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#readByPhone(String)                                 
     */
    @Override
    public Employee getEmployeeByPhone(String phone) throws DBException { 
        Employee employee = null;
        try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("phone", phone));
            employee = (Employee) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DBException("Exception while getting employee with phone" 
                                      + phone, e);
        }
        return employee;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#retrieveEmployeeById(int)                              
     */
    @Override
    public Map<String, Object> retrieveEmployeeById(int id) throws AppException
    {
        Connection connection = null;
        PreparedStatement statement = null;
        int resultCount = 0;
        Map<String, Object> employee = new HashMap<>();
        try {
            connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement("SELECT name FROM employee " 
                            + "WHERE id = ? AND is_deleted = 0;");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                employee.put("name", resultSet.getString(1));
                employee.put("projectId", resultSet.getInt(2));
            }
        }  catch (SQLException sqlException) {
            throw new AppException("Exception while retriving employee " 
                                           + "with id = " + id, sqlException);
        }
        finally {
            ConnectionFactory.close(connection, statement);
        }
        return employee;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#assignProjectToEmployee(
     *      int)                                
     */
    @Override
    public int assignProjectToEmployee(int employeeId, int projectId) throws 
        AppException
    {
        Connection connection = null;
        PreparedStatement statement = null;
        int resultCount = 0;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement("INSERT INTO " 
                             + "employee_project values(?, ?);");
            statement.setInt(1, employeeId);
            statement.setInt(2, projectId);     
            resultCount = statement.executeUpdate();
        } catch (SQLException sqlException) {
            throw new AppException("Exception while assigning employee to" 
                                       + " project.", sqlException);
        }   
        finally {
            ConnectionFactory.close(connection, statement);
        }    
        return resultCount;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#isEmployeeExists(int)                
     */
    @Override
    public boolean isEmployeeExists(int id) throws AppException {
        Connection connection = null;
        PreparedStatement statement = null;
        boolean result = false;
        try {
            connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement("SELECT name FROM employee" 
                            + " WHERE id = ? AND is_deleted = 0;");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            result = resultSet.next();
        } catch (SQLException sqlException) {
            throw new AppException("Exception while trying to read with id = " 
                                       + id, sqlException);
        }
        finally {
            ConnectionFactory.close(connection, statement);
        }
        return result;    
    }
     
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#retrieveEmployeesByProject(
     *      int)                                  
     */
    @Override
    public List<Employee> retrieveEmployeesByProject(int id) throws AppException 
    {
        List<Employee> employees = new ArrayList<>();
        try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Employee.class);
            //criteria.add(Restrictions.eq("id", id));
            employees = criteria.list();
        } catch (HibernateException e) {
            throw new AppException("Exception while retriving employees by" 
                                       + " project.", e);
        }
        return employees;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.EmployeeDAO#getEmployeeById(id)                   
     */
    @Override
    public Employee getEmployeeById(int id) throws DBException {
        Employee employee = null;
        try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("id", id));
            criteria.add(Restrictions.eq("isDeleted", 0));
            employee = (Employee) criteria.uniqueResult();
        } catch(HibernateException e) {
            throw new DBException("Exception while trying to retrieve Employee " 
                                            + "with id " + id, e);
        }
        return employee;        
    }
}

