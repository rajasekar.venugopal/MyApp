package com.ideas2it.empresa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.common.ConnectionFactory;
import com.ideas2it.empresa.dao.AddressDAO;
import com.ideas2it.empresa.model.Address;

/**
 * The AddressDAOImpl is the implementation of AddressDAOImpl interface it 
 * implements all methods to interact with the record and acts as an 
 * intermediate for retrieving and storing data into or from the record.
 *
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class AddressDAOImpl implements AddressDAO { 
    private static Configuration config = new Configuration().configure("hibernate.cfg.xml");
	private static SessionFactory factory = config.buildSessionFactory();

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.AddressDAO#addAddress(Address)                                  
     */
    public int addAddress(Address address) throws AppException {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(address);
        transaction.commit();
        session.close();           
        return 1;       
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.AddressDAO#addAddress(Address)
     */
    @Override
    public int updateAddress(Address address) throws AppException {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(address);
        transaction.commit();
        session.close();           
        return 1;      
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.AddressDAO#addAddress(Address)
     */
    @Override
    public boolean isTypeExists(Address address) throws AppException {
        boolean  isExists = false;
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM designation WHERE type = :address.type");
        query.setParameter("type",address.getType());
        int result = query.executeUpdate();
        if(result > 1) {
            isExists = Constant.TRUE;
        }      
        return isExists; 
    }   
}
