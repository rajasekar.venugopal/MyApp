package com.ideas2it.empresa.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.common.ConnectionFactory;
import com.ideas2it.empresa.dao.ClientDAO;
import com.ideas2it.empresa.model.Address;
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.common.SessionGenerator;

/**
 * The ClientDAOImpl is the implementation of ClientDAO interface it implements 
 * all methods to interact with the record and acts as an intermediate for 
 * retrieving and storing data into or from the record.
 *
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class ClientDAOImpl implements ClientDAO { 
    private static SessionFactory factory = SessionGenerator.getFactory();     

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#insert(Client)
     */
    @Override
    public int insert(Client client) throws AppException {
        int id;
        SessionFactory factory = SessionGenerator.getFactory();
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();
            id = (Integer) session.save(client);    
            session.flush();
            transaction.commit();
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to insert project"
                                       , e); 
        }
        return id;  
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#retriveAllClients()
     */
    @Override
	public List<Client> retriveAllClients() throws AppException {
        List<Client> clients = null;
		try (Session session = factory.openSession()) {
            Criteria criteria = session.createCriteria(Client.class);
            criteria.add(Restrictions.eq("isDeleted", 0));
            clients = criteria.list();
		} catch (HibernateException e) {
            throw new AppException("Exception while trying to retrieve all " 
                                      + "projects ", e);           
        }
		return clients;
	}
    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#delete(int)
     */
    @Override
    public void delete(Client client) throws DBException {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();            
            session.update(client);
            transaction.commit();
        } catch(HibernateException e) {
            throw new DBException("Exception while trying to delete client", e);
        }
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#updateEmailById(int, String)                
     */
    @Override
    public int update(Client client) throws DBException {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();         
            session.update(client);
            transaction.commit();
        } catch(HibernateException e) {
            throw new DBException("Exception while trying to updating client",
                                      e);
        }
        return 1;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#isClientExists(int)
     *
    @Override
    public boolean isClientExists(int clientId) throws AppException {
        Connection connection = null;
        PreparedStatement statement = null;
        boolean result = false;
        try {
            connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement("SELECT name FROM client" 
                            + " WHERE id = ? AND is_deleted = 0;");
            statement.setInt(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            result = resultSet.next();
        } catch (SQLException sqlException) {
            throw new AppException("Exception while trying to read with id = " 
                                       + clientId, sqlException);
        }
        finally{
            ConnectionFactory.close(connection, statement);
        }
        return result;    
    }/

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#isEmailExists(String)                
     */
    @Override
    public boolean isEmailExists(String email) throws DBException {
        boolean isExists = false;
        try (Session session = factory.openSession()) { 
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Client> criteria = builder.createQuery(Client.class); 
            Root<Client> root = criteria.from(Client.class);
            criteria.select(root);  
            criteria.where(builder.equal(root.get("email"), email));
            List<Client> clients = session.createQuery(criteria).getResultList();
            if(clients.size() > 0) {
                isExists = true;
            }
        } catch (HibernateException e) {
            throw new DBException("Exception while reading client with" 
                                       + "email = " + email, e);
        }
        return isExists;         
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.dao.ClientDAO#getClientById(id)                   
     */
    @Override
    public Client getClientById(int id) throws DBException {
        Client client = null;
        try (Session session = factory.openSession()) {
            client = session.get(Client.class, new Integer(id));
        } catch(HibernateException e) {
            throw new DBException("Exception while trying to retrieve client " 
                                            + "with id " + id, e);
        }
        return client;        
    }
}
