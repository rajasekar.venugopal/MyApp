package com.ideas2it.empresa.dao;

import java.util.List;

import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;

/**
 * The DesignationDAO exposes read operation related to the Designation.
 * 
 * @author Rajasekar
 * created on 21/07/2017
 */
public interface DesignationDAO {
    /**
     * Retrieves all the designations from the designations record.
     * @return returns all the designations as a list.
     * @throws AppException if designations cannot be retrieved.
     */
    List<Designation> retireveAllDesignations() throws DBException ; 
    
    /**
     * Retrieves Designation for the given id.
     * @return returns deignation for the given id.
     * @throws AppException if designations cannot be retrieved.
     */
    Designation retrieveDesignationById(int id) throws DBException;
}
