package com.ideas2it.empresa.dao;

import java.util.List;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Client;

/**
 * The ClientDAO exposes all the CRUD operations related to the client.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface ClientDAO {

    /**
     * Inserts client data into the client record and returns whether 
     * inserted successfully or not, along with the client id of the client if 
     * inserted successfully else failure message alone.
     * @param client - Client object whichs contains the details of the client.
     * @return one of the client is inserted successfully else zero,
     * @throws AppException if the client cannot be inserted.
     */
    int insert(Client client) throws AppException;

    /**
     * Retrieves all clients from the record and sets it to client 
     * object and returns a list of client object.
     * @return clients list containing client object.
     * @throws AppException if clients cannot be retrieved.
     */
	List<Client> retriveAllClients() throws AppException;

    /**
     * Deletes the given client from the client record.
     * @param client client to be deleted from the client record. 
     * @throws AppException if client cannot be deleted.
     */
    void delete(Client client) throws DBException;

    /**
     * Updates the given client from the client record.
     * @param client client to be deleted from the client record. 
     * @throws AppException if client cannot be deleted.
     */
    int update(Client client) throws DBException;

    /**
     * Checks whether the client is existing for the given id and returns true 
     * if client is existing in the record false otherwise.
     * @param clientId client id which is unique for each project.
     * @return true if project is existing false otherwise
     * @throws AppException if project cannot be checked whether exists.
     *
    boolean isClientExists(int clientId) throws AppException;

    /**
     * Retrieves client from record with the email address.
     * @param email email address of the client used to communicate him/her  
     * @return - true if email address is existing and related to any client,  
     *               else false.
     * @throws DBException if client cannot be retrieved from record.                 
     */
    boolean isEmailExists(String email) throws DBException;

    /**
     * Retrieves client for the given id and returns it.
     * @param id id of the client which is unique to every client.
     * @return Client client for the given id.
     * @throws AppException if no client is found for the given id.
     * @throws DBException if client for the given id cannot be retrieved.
     */
    Client getClientById(int id) throws AppException, DBException;
}
