package com.ideas2it.empresa.dao;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;
import com.ideas2it.empresa.model.Employee;

/**
 * The EmployeeDAO exposes all the CRUD operations related to the employees.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface EmployeeDAO {

    /**
     * Retrieves all record from the employees record and sets it to employee 
     * object and returns a list of employee object.
     * @return List employees list containing employees objects.
     * @throws AppException if employees cannot be retrieved from record.                 
     */
    List<Employee> retrieveEmployees() throws AppException;

    /**
     * Inserts employee data into the employee record and returns whether 
     * inserted successfully or failed to insert. 
     * @param employee Employee object containing all employee data.
     * @return zero if not inserted otherwise returns the id of the 
     *               inserted record.
     * @throws AppException if a employee cannot be inserted to record.                 
     */
    int insert(Employee employee) throws DBException;

    /**
     * Deletes employee from the employees record with id.
     * @param employee employee to be deleted from the record
     * @return zero if no record is deleted ,else one if deleted.
     * @throws AppException if employee cannot be deleted from record.                 
     */
    void delete(Employee employee) throws DBException;

    /**
     * Updates employee name in the employee record with the given name using 
     * the id to find the record.
     * @param   id - id of the employee which is unique to every employee.
     * @param name - new name to updated in the record.
     * @return int - zero if no record is updated, else one if updated.
     * @throws AppException if employee cannot be deleted from record.                 
     */
    void updateNameById(Employee employee) throws DBException;

    /**
     * Updates employee's email in the employee record with the given email 
     * using the id to find the record.
     * @param id - id of the employee which is unique to every employee.
     * @param email - new email to updated in the record.
     * @return int - zero if no record is updated, one if updated.
     * @throws AppException if email cannot be updated to record.                 
     */
    void updateEmailById(Employee employee) throws DBException;

    /**
     * Updates employee's phone in the employee record with the given phone 
     * number using the id to find the record.
     * @param   id - id of the employee which is unique to every employee.
     * @param phone - new phone number to updated in the record.
     * @return int - zero if no record is updated, one if updated.
     * @throws AppException if phone cannot be updated to record.                 
     */
    void updatePhoneById(Employee employee) throws DBException;

    /**
     * Checks whether email is existing in the employee record for any employee.
     * @param email email address of the employee used to communicate him/her
     * @return  true if phone number exists else false.  
     * @throws AppException if employee cannot be retrieved from record.                 
     */
    Employee getEmployeeByEmail(String email) throws DBException;

    /**
     * Checks whether phone number is existing in the employee record for any 
     * employee.  
     * @param phone phone number of the employee used to communicate him/her
     * @return - true if phone number exists else false.  
     * @throws AppException if employee cannot be retrieved from record.                 
     */
    Employee getEmployeeByPhone(String phone) throws DBException;

    /**
     * Retrieves an employee from the record by employee id and returns the 
     * employee name and project id. 
     * @param   id - id of the employee which is unique to every employee.
     * @return map - containing employee name and project id.
     * @throws AppException if employees cannot be retrieved from 
     *                  record.                 
     */
    Map<String, Object> retrieveEmployeeById(int id) throws AppException;

    /**
     * Assigns a project to the employee with the given project id and employee 
     * id.
     * @param employeeId employee id to update project id.
     * @param projectId project id to be added in employee tabel.
     * @throws AppException if employee cannot be assigned to project.
     * @return zero if the employee is not assigned to the project else one.                 
     */
    int assignProjectToEmployee(int employeeId, int projectId) throws 
        AppException;

    /**
     * Retrieves all employees from the record working in the project identified 
     * by project id and returns it as a list of employee object.
     * @param id of the project which is unique and used to find the project.
     * @return List - returns the list of all employee in a project.
     * @throws AppException if employees cannot be retrieved from record.                  
     */
    List<Employee> retrieveEmployeesByProject(int id) throws 
        AppException;

    /**
     * Checks whether the emploeye is existing for the given id and returns true 
     * if employee is existing in the record false otherwise.
     * @param id project id which is unique for each project.
     * @return true if project is existing false otherwise
     * @throws AppException if employee cannot be checked.
     */
    boolean isEmployeeExists(int id) throws AppException;

    /**
     * Retrieves employee for the given id and returns it.
     * @param id id of the employee which is unique to every employee.
     * @return Employee employee for the given id.
     * @throws AppException if no employee is found for the given id.
     * @throws DBException if employee for the given id cannot be retrieved.
     */
    Employee getEmployeeById(int id) throws AppException, DBException;
}
