package com.ideas2it.empresa.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;

/**
 * The AddressService interface exposes all the create, update and business 
 * operation of the Address.
 * 
 * @author Rajasekar
 * created on 19/07/2017
 */
public interface AddressService {

    /**
     * Adds address to address record and returns success message if added 
     * successfully else failure message. 
     * @param address - address, postal address of the owner which can be used to 
     *          communicate him / her.
     * @return result returns id of the address if added successfully else zero 
     * @throws AppException if the address cannot be added to the owner. 
     */
    String addAddress(Address address) throws AppException;

    /**
     * Takes new address of the owner, updates the address to the owner and 
     * returns success message if added successfully else failure message. 
     * @param address - address, postal address of the owner which can be used to 
     *          communicate him / her.
     * @return result - returns id of the address if updated successfully else 
     *                   zero 
     * @throws AppException if the address cannot be added to the owner. 
     */
    String updateAddress(Address address) throws AppException;
     
    /**
     * Checks whether the given type of the address is available for the    
     * particular owner and returns true if available else false.
     * @param address - address, postal address of the owner which can be use to
     *              communicate him / her.
     * @return returns whether the type is available or not and corresponding 
     *         message.
     * @throws AppException address type's status cannot be checked.
     */
    Map<String, String> getTypeStatus(Address address) throws AppException;

    
}

