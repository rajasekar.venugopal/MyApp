package com.ideas2it.empresa.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Designation;

/**
 * The ProjectService interface exposes the read operation of the project.
 * 
 * @author Rajasekar
 * created on 21/07/2017
 */
public interface DesignationService { 
    /**
     * Retrieves all desingations from the designation record.
     * @return list of designations 
     * @throws AppException if designations cannot be retireved.
     */
    List<Designation> retrieveAllDesignations() throws AppException, DBException;

    /**
     * Retrieves Designation for the given id.
     * @param id id of the deignaiton to be retrieved
     * @return returns deignation for the given id.
     * @throws AppException if designations cannot be retrieved.
     */
    Designation retrieveDesignationById(int id) throws AppException, DBException;
}
