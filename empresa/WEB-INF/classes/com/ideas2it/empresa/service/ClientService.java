package com.ideas2it.empresa.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Client;

/**
 * The ClientService interface exposes all the CRUD and business operation of 
 * the client.
 * 
 * @author Rajasekar
 * created on 14/07/2017
 */
public interface ClientService {

    /**
     * Takes client details and adds a client to the client record. 
     * @param client client to be added to the client record
     * @return  map with id of the inserted client and status message whether 
     *          inserted or not. 
     * @throws AppException if client cannot be added.                  
     */ 
    Map<String, Object> addClient(Client client)throws AppException,
        DBException;

    /**
     * gets all clients from record and returns list of the clients.
     * @return List of all Client model's objects 
     * @throws AppException if client cannot be retrieved.
     */
    List<Client> getAllClients() throws AppException;
    
    /**
     * Deletes the given client from the client record.
     * @param client client to be deleted from the client record. 
     * @return message success message when client client is deleted.
     * @throws AppException if client is cannot be deleted.
     */
    String deleteClient(Client client) throws AppException, DBException;

    /**
     * Updates client's email and returns success message if email is updated
     * successfully else error message.   
     * @param id id of the client which is unique to every client. 
     * @param email Updated email of the client	 
     * @return status Success message if email is updated successfully, 
     *                     failure message other wise.
     * @throws AppException if email cannot be updated.
     */
    String updateEmail(Client client) throws AppException, DBException;

    /** 
     * Validates the id if not valid returns failure message as map otherwise 
     * checks if client is existing with the id and returns success or failure 
     * map and corresponding message.  
     * @param id - id of the client which is unique to every client.
     * @return status status either success or failure message and whether 
     *              client is existing or not 
     * @throws AppException if cannot retrive a client.                 
     */
    Map<String, String> hasClientForId(String id) throws AppException, 
        DBException;

    /**
     * Gets client for the given id and returns it.
     * @param id id of the client which is unique to every client.
     * @return Client client for the given id.
     * @throws AppException if no client is found for the given id.
     * @throws DBException if client for the given id cannot be retrieved.
     */
    Client getClientById(int id) throws AppException, DBException;
}
