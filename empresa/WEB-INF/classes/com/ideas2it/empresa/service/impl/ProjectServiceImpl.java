package com.ideas2it.empresa.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.dao.impl.ProjectDAOImpl;
import com.ideas2it.empresa.dao.ProjectDAO;
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.model.Employee;
import com.ideas2it.empresa.model.Project;
import com.ideas2it.empresa.service.ProjectService;

/**
 * <p>ProjectServiceImpl is the implementation of the ProjectService interface 
 * it implements all the methods in the interface to process all the business 
 * logic of the project, gets the input for operation validates as the first 
 * step then the actual operation is carried as invalid data should not  
 * processed and to ensure the reliability of the data in the data store. 
 *  
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDAO projectDAOImpl = new ProjectDAOImpl();

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#addProject(String, 
     *          String, int)                                  
     */ 
    @Override
    public Map<String, Object> addProject(String name, String description, 
        int clientId) throws AppException 
    {
        Map<String, Object> status = new HashMap<String, Object>();
        String statusMessage;
        int noOfAffectedRows = projectDAOImpl.insert(new Project(name, 
                                   description, new Client(clientId)));
        if(Constant.ZERO < noOfAffectedRows) {
            statusMessage = "Inserted successfully and project id is "
                                + noOfAffectedRows;
            status.put("ProjectId", noOfAffectedRows);
            status.put(Constant.ERROR_STATE, Constant.SUCCESS);
        } else {
            statusMessage = "Not inserted.";
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
        }   
        status.put(Constant.MESSAGE, statusMessage);
        return status;
    }
    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getAllProjects()                                                  
     */
    @Override
    public List<Project> getAllProjects() throws AppException {
        return projectDAOImpl.retriveAllProjects();
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#deleteProject(int)                                                 
     */
    @Override
    public String deleteProject(int id) throws AppException {
        String statusMessage;   
        Project project = new Project(id);
        project.setIsDeleted(1);
        if(Constant.ZERO < projectDAOImpl.delete(project)) {
            statusMessage = "Deleted successfully.";
        } else {
            statusMessage = "Not deleted";
        }
        return statusMessage;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#hasProjectForId(     
     *      String)                                                   
     */
    @Override
    public Map<String, String> hasProjectForId(String id) throws AppException { 
        Map<String, String> status = new HashMap<>();
        if(CommonUtil.isNumeric(id)) {
            Project project = new Project(Integer.parseInt(id));
            if(projectDAOImpl.isProjectExists(project)) {
                status.put(Constant.ERROR_STATE, Constant.SUCCESS);
                status.put(Constant.MESSAGE, "Project found.");
            } else {
                status.put(Constant.ERROR_STATE, Constant.FAILURE);
                status.put(Constant.MESSAGE, "No Project found.");
            } 
        } else {
                status.put(Constant.ERROR_STATE, Constant.FAILURE);
                status.put(Constant.MESSAGE, Constant.INVALID_NO);
        }                 
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getAllProjectsByClientId(int)                                               
     */
    @Override
    public List<Project> getAllProjectsByClientId(int clientId) throws 
        AppException 
    {
        return projectDAOImpl.retrieveAllProjects(clientId);
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getProjectById(
     *      int, int)                                                  
     */
    @Override
    public Map<String, Object> getProjectById(int projectId, int clientId) 
        throws AppException 
    {
        Map<String, Object> status = new HashMap<>();
        Project project = projectDAOImpl.retrieveProjectById(projectId, 
                              clientId);
        if(project != null) {
            status.put(Constant.PROJECT, project);
            status.put(Constant.ERROR_STATE, Constant.SUCCESS);
            status.put(Constant.MESSAGE, "Project found.");
        } else {
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
            status.put(Constant.MESSAGE, "Project not found.");
        }
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getProjectByName(
     *      String, int)                                                  
     */
    @Override
    public Map<String, Object> getProjectByName(String projectName, 
        int clientId) throws AppException 
    {
        Map<String, Object> status = new HashMap<>();
        Project project = projectDAOImpl.retrieveProjectByName(projectName, 
                                                                   clientId);
        if(project != null) {
            status.put(Constant.PROJECT, project);
            status.put(Constant.ERROR_STATE, Constant.SUCCESS);
            status.put(Constant.MESSAGE, "Project found.");
        } else {
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
            status.put(Constant.MESSAGE, "Project not found.");
        }
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getProjectById(int)                   
     */ 
    public Project getProjectById(int id) throws AppException, DBException {
        Project project = projectDAOImpl.getProjectById(id);
        if(null == project) {
            throw new AppException("No project Found ");
        }
        return project;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#isProjectExists(int)                                  
     */
    @Override
    public boolean isProjectExists(int projectId) throws AppException {
        return projectDAOImpl.isProjectExists(new Project(projectId));
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#getNoOfProjects(int)                   
     */ 
    public int getNoOfProjects(int clientid) throws AppException {
        return projectDAOImpl.getNoOfProjects(clientid);
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ProjectService#assignProjectToEmployee(Project, Employee)                   
     */ 
    public String assignProjectToEmployee(Project project, Employee employee) 
        throws DBException 
    {
        List<Employee> employees = project.getEmployees();
        employees.add(employee);
        project.setEmployees(employees);
        projectDAOImpl.assignProjectToEmployee(project);
        return "Assigned successfully";
    }
}
