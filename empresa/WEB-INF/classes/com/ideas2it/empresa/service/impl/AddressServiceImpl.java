package com.ideas2it.empresa.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.dao.AddressDAO;
import com.ideas2it.empresa.dao.impl.AddressDAOImpl;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.model.Address;
import com.ideas2it.empresa.service.AddressService;

/**
 * <p>AddressServiceimpl is the implementation of the AddressService interface  
 * it implements all methods in AddressService to process all the business logic  
 * of the address, gets the input for operation validates as the first step then 
 * the actual operation is carried as invalid data should not processed and to 
 * ensure the reliability of the data in the data store.  
 *  
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class AddressServiceImpl implements AddressService { 
    AddressDAO addressDAOImpl = new AddressDAOImpl();    

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.AddressService#addAddress(Address)                   
     */
    public String addAddress(Address address) throws AppException {
        String status = new String();
        if(Constant.ZERO < addressDAOImpl.addAddress(address)) {
            status = "Address added to owner successfully.";
        } else {
            status = "Address was not added.";        
        } 
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.AddressService#updateAddress(
     *          Address)                   
     */
    public String updateAddress(Address address) throws AppException {
        String status = new String();
        if(Constant.ZERO < addressDAOImpl.updateAddress(address)) {
            status = "Address updated to owner successfully.";
        } else {
            status = "Address was not added.";        
        } 
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.AddressService#getTypeStatus(Address)                   
     */
    public Map<String, String> getTypeStatus(Address address) throws 
        AppException 
    {
        Map<String, String> status = new HashMap<>();
        if(addressDAOImpl.isTypeExists(address)) {
            status.put(Constant.MESSAGE, address.getType() + " address for the" 
                           + " owner is already exists");
            status.put(Constant.ERROR_STATE, Constant.SUCCESS);
        } else {
            status.put(Constant.MESSAGE, address.getType() + " address for the" 
                           + " owner does not exists");
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
        } 
        return status;
    }
}
