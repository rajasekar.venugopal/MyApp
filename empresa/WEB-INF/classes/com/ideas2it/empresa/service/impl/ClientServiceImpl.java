package com.ideas2it.empresa.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideas2it.empresa.common.CommonUtil;
import com.ideas2it.empresa.common.Constant;
import com.ideas2it.empresa.dao.ClientDAO;
import com.ideas2it.empresa.dao.impl.ClientDAOImpl;
import com.ideas2it.empresa.exception.AppException;
import com.ideas2it.empresa.exception.DBException;
import com.ideas2it.empresa.model.Client;
import com.ideas2it.empresa.service.ClientService;

/**
 * <p>ClientServiceimpl is the implementation of the ClientService interface it 
 * implements all methods in ClientService to process all the business logic of 
 * the client, gets the input for operation validates as the first step then the
 * actual operation is carried as invalid data should not processed and to 
 * ensure the reliability of the data in the data store.
 *  
 * @author Rajasekar 
 * created on 2017/07/07
 */
public class ClientServiceImpl implements ClientService {
    private ClientDAO clientDAOImpl = new ClientDAOImpl();
    
    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#addClient(String, 
     *      String)                   
     */ 
    @Override
    public Map<String, Object> addClient(Client client) throws 
        AppException, DBException 
    {
        Map<String, Object> status = new HashMap<String, Object>();
        StringBuilder statusMessage = new StringBuilder();
        boolean isValid = clientValidator(client.getName(), client.getEmail(), statusMessage);   
        boolean emailExists = isEmailExists(client.getEmail());
        if(isValid) {
            if(!emailExists) {
                int clientId = clientDAOImpl.insert(client);
                if(Constant.ZERO < clientId) {
                    statusMessage.append("Inserted successfully and client id is "
                                + clientId);
                    status.put(Constant.CLIENT_ID, clientId);
                    status.put(Constant.ERROR_STATE, Constant.SUCCESS);
                } else {
                    statusMessage.append("Not inserted.");
                    status.put(Constant.ERROR_STATE, Constant.FAILURE);
                } 
            } else {
                statusMessage.append("Email already exists.");
                status.put(Constant.ERROR_STATE, Constant.FAILURE);
            }   
        } else {
            statusMessage.append(Constant.INVALID_NO);
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
        }   
        status.put(Constant.MESSAGE, statusMessage.toString());
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#getAllClients()                   
     */
    @Override
    public List<Client> getAllClients() throws AppException {
        List<Client> clients = clientDAOImpl.retriveAllClients();
        if(clients.isEmpty()) {
            throw new AppException("Client record is empty");
        }
        return clients;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#deleteClient(int)                   
     */
    @Override
    public String deleteClient(Client client) throws AppException, DBException {
        client.setIsDeleted(1);
        clientDAOImpl.delete(client);
        return "Deleted successfully.";
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#hasClientForId(String)                   
     */
    @Override
    public Map<String, String> hasClientForId(String id) throws AppException, 
        DBException 
    { 
        Map<String, String> status = new HashMap<>();
        if(CommonUtil.isNumeric(id)){
            if(null != clientDAOImpl.getClientById(Integer.parseInt(id))) {
                status.put(Constant.ERROR_STATE, Constant.SUCCESS);
                status.put(Constant.MESSAGE, "client Found.");
            } else {
                status.put(Constant.ERROR_STATE, Constant.FAILURE);
                status.put(Constant.MESSAGE, "No client found.");
            }                
        } else {
            status.put(Constant.ERROR_STATE, Constant.FAILURE);
            status.put(Constant.MESSAGE, Constant.INVALID_NO);   
        }
        return status;
    }

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#updateEmailById(String, String)                   
     */
    @Override
    public String updateEmail(Client client) throws AppException, 
        DBException 
    {
        String statusMessage = new String();
        if(CommonUtil.isEmail(client.getEmail())) {
            int resultCount = clientDAOImpl.update(client);    
            if(Constant.ZERO < resultCount) {
                statusMessage = "Updated.";
            } else {
                throw new AppException("Not updated.");
            }    
        } else {
            throw new AppException("Invalid email.");
        }        
        return statusMessage;
    } 

    /**
     * (non-javadoc)
     *
     * @see com.ideas2it.empresa.service.ClientService#getClientById(id)                   
     */
    @Override
    public Client getClientById(int id) throws AppException, DBException {
        Client client = clientDAOImpl.getClientById(id);
        if(null == client) {
            throw new AppException("No Employee Found ");
        }
        return client;
    }

    /**
     * <p>Takes email and checks whether email is already exists for another 
     * client if exists returns true, false otherwise.
     * @param email - email address to be searched.
     * @return - true if phone exists, false otherwise.
     * @throws - AppException if email cannot be readed.
     */
    private boolean isEmailExists(String email) throws DBException
    {
        return clientDAOImpl.isEmailExists(email);
    } 

    /**
     * <p>Validates client details and returns true if all details are valid, 
     * false otherwise.
     * @param name -  name to be validated 
     *                eg.steven3 is not valid as it contains number.
     * @param email - email to be validated 
     * @param statusMessage - message to append errors when invalid data 
     *                          is checked.
     * @return - true if all details are valid and false otherwise. 
     * @throws - AppException if date is not valid.
     */ 
    private boolean clientValidator(String name, String email, 
        StringBuilder statusMessage) throws AppException 
    {
        boolean successState = Constant.TRUE;
        if(!CommonUtil.isLiteral(name)) {
            statusMessage.append("Name should contain only character");
            successState = Constant.FALSE;
        }
        if(!CommonUtil.isEmail(email)) {
            statusMessage.append("Email invalid example(example@domain.com)");
            successState = Constant.FALSE;
        }
        return successState;
    }
}
