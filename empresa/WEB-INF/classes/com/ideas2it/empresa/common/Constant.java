package com.ideas2it.empresa.common;

/**
 * Contains constant variables.
 *
 * @author Rajasekar 
 * created on 2017/07/11
 */
public final class Constant { 
    public static final int ZERO = 0;
    public static final int TWENTY = 20;
    public static final String ERROR_STATE = "State";
    public static final String PROJECT_STATE = "ProjectState";
    public static final String MESSAGE = "Message";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final boolean TRUE = true;
    public static final boolean FALSE = false; 
    public static final String YES = "yes";
    public static final String NO = "NO";
    public static final String INVALID_NO = "invalid no";
    public static final String INVALID_EMAIL = "invalid email";
    public static final String PROJECT_ID = "ProjectId";
    public static final String PROJECT = "project";
    public static final String NO_OF_PROJECTS = "noOfProjects";
    public static final String CLIENT_ID = "clientId"; 
    public static final String CLIENT = "client";
    public static final String EMPLOYEE = "employee";
    public static final String ID = "id";
    public static final String INVALID_CHOICE = "Sorry, invalid choice.";
    public static final String PRIMARY = "primary";
    public static final String SECONDARY = "secondary";
}
