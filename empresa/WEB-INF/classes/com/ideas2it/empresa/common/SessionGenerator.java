package com.ideas2it.empresa.common;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

/**
 * 
 */
public class SessionGenerator {
    private static SessionGenerator sessionGenerator = new SessionGenerator(); 
    private static SessionFactory factory = null;
    
    /**
     *
     */
    private SessionGenerator() {

    }
    
    /**
     *
     */
    private SessionFactory createFactory() {
        if(factory == null) {
            Configuration config = new Configuration().configure("hibernate.cfg.xml");
	        factory = config.buildSessionFactory();
        }    
        return factory;
    }
    
    /**
     *
     */
    public static SessionFactory getFactory() {
        return sessionGenerator.createFactory();
    }
}
