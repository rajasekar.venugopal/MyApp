package com.ideas2it.empresa.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
/*
import com.mysql.jdbc.Driver;

/**
 * The ConnectionFactory is responsible is responsible for the creating a 
 * connection to the database and closing the services. It encapsulates the 
 * process of creating the connection so that it can maintain a single 
 * connection to datastore.
 */
public class ConnectionFactory {
    private static ConnectionFactory connectionFactory = new ConnectionFactory(); 
    private final String URL  = "jdbc:mysql://localhost:3306/employee_management"
                                  +"?verifyServerCertificate=false&useSSL=true";
    private final String USER = "root";
    private final String PASSWORD = "ubuntu";

    
    /**
     * Private constructor to ensure that no object can be created outside the 
     * class and only one connection is maintained. 
     */
    private ConnectionFactory() {

    }

    /**
     * Creates connection to the database and returns the connection object.
     * @return connection   Connection to the database.
     * @exception   SQLException if cannot get connection of the database.
     */
    private Connection createConnection() throws SQLException{
        Connection connection = null;
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
        return connection;
    }
    
    /**
     * Returns a connection object.
     * @return connection   Connection object to the database. 
     * @throws SQLException if cannot get connection of the database.
     */
    public static Connection getConnection() throws SQLException{
        return connectionFactory.createConnection();
    }

    /**
     * Closes the Connection and Statement established with the database. 
     * @param connection - connection object which is to be closed.
     * @param statement - statement object which is to be closed.
     */ 
    public static void close(Connection connection, Statement statement) 
    {
        try {
		    if (statement != null) {
                statement.close();
		    }
		    if (connection != null) {
                connection.close();
		    }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
