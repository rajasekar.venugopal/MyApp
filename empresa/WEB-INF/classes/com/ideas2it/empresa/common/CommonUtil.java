package com.ideas2it.empresa.common;

import java.time.format.DateTimeParseException;
import java.time.LocalDate;
import java.time.Period;

/**
 * Validates numeric, phone, date format, literal, email and 
 * to calculate age from the given date.
 *  
 * @author Rajasekar 
 * created on 2017/07/01
 */
public final class CommonUtil {
    /**
     * Private constructor to prevent creating object.
     */
    private CommonUtil() {

    }    

    /**
     * Returns true if the text contains all numeric value.
     * @param text    text to be validated
     * @return true if the text has non numeric, false otherwise.
     */
    public static boolean isNumeric(String text) {
        for(int index = 0; index < text.length(); index++) {
            if(text.charAt(index) < 48 || text.charAt(index) > 57){
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if the text is a valid phone number.
     * @param phone    phone no to be validated.
     * @return  true if the text is a valid phone number, false otherwise.
     */
    public static boolean isPhone(String phone) {
        if(10 != phone.length()) {
            return false;
        }
        for(int index = 0; index < phone.length(); index++) {
            if(phone.charAt(index) < 48 || phone.charAt(index) > 57){
                return false;
            }
        }
        return true; 
    }

    /**
     * Returns true if the text is a valid literal.
     * @param text    text to be validated.
     * @return  true if the text is valid literal. false otherwise.  
     */
    public static boolean isLiteral(String text) {
        for(int index = 0; index < text.length(); index++) {
            if((text.charAt(index) < 65 || text.charAt(index) > 90) 
                    && (text.charAt(index) < 97 || text.charAt(index) > 122)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns true if the text is a valid email.
     * @param email    email to be validated.   
     * @return true if the email is a valid email, false otherwise.  
     */
    public static boolean isEmail(String email) {
        if (!(email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)"
                                       + "+[\\w]+[\\w]$"))) {
            return false;
        }
        return true;
    }

    /**
     * Return true if the text is in a valid date format
     * @param date    date whose format is to be validated.
     * @return true if the date is in a correct format, false otherwise.
     */
    public static boolean isDate(String date) throws DateTimeParseException {
        if (!(date.matches("^[0-9]{4}-(1[0-2]|0[1-9])"
                                           + "-(3[01]|[1|2][0-9]|0[1-9])$"))) {
            return false;        
        }
        return true;
    }

    /**
     * Calculates age and returns the age for the input date.
     * @param value    date for which the age to be calculated.
     * @return age      the calculated age from the input date .
     */     
    public static int calculateAge(String value) {
        LocalDate date = LocalDate.parse(value);
        LocalDate today = LocalDate.now();  
        return Period.between(date, today).getYears();
    }
}
